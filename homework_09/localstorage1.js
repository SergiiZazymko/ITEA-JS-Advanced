// Задание 1:
// Написать скрипт, который по клику на кнопку рандомит цвет и записывает его в localStorage
// После перезагрузки страницы, цвет должен сохранится.

window.addEventListener('DOMContentLoaded', e => {

    document.getElementById('button').addEventListener('click', e => {
        localStorage.setItem('color', getRandomColor());
    });

    let color = localStorage.getItem('color');

    if (color !== null) {
        document.body.style.background = color;
    }

    /**
     *
     * @param min
     * @param max
     * @returns {number}
     */
    function getRandomIntInclusive(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);

        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    /**
     *
     * @returns {string}
     */
    function getRandomColor() {
        let randomColor = '#';
        for (let i = 0; i < 3; i ++) {
            let color = getRandomIntInclusive(0, 255).toString(16);
            if (color.length < 2) {
                color = '0' + color;
            }
            randomColor += color;
        }

        return randomColor;
    }
});
