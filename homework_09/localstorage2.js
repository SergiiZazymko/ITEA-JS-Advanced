/*

  Задание 2:
    Написать форму логина (логин пароль), которая после отправки данных записывает их в localStorage.
    Если в localStorage есть записть - На страниче вывести "Привет {username}", если нет - вывести окно
    логина.

    + бонус, сделать кнопку "выйти" которая удаляет запись из localStorage и снова показывает форму логина
    + бонус сделать проверку логина и пароля на конкретную запись. Т.е. залогинит пользователя если
    он введет только admin@example.com и пароль 12345678
*/

window.addEventListener('DOMContentLoaded', () => {

    document.querySelector('#login_form > form').addEventListener('click', loginAction);
    document.querySelector('#welcome > button').addEventListener('click', logoutAction);

    login();

    /**
     *
     */
    function login() {
        if (user = auth()) {
            document.getElementById('login_form').classList.add('hidden');
            document.getElementById('welcome').classList.remove('hidden');
            document.querySelector('#welcome > h3').innerHTML = `Hello ${user.name}`;
        } else {
            document.getElementById('login_form').classList.remove('hidden');
            document.getElementById('welcome').classList.add('hidden');
        }
    }

    /**
     *
     * @returns {*}
     */
    function auth() {
        let user = localStorage.getItem('user');
        if (null !== user) {
            user = JSON.parse(user);
            if ('admin@example.com' === user.name && '12345678' === user.password) {
                return user;
            }
        }
        return false;
    }

    /**
     *
     * @param e
     */
    function loginAction(e) {
        e.preventDefault();
        let user = {
            name : this.name.value,
            password : this.password.value,
        };
        localStorage.setItem('user', JSON.stringify(user));
        login();
    }

    /**
     *
     * @param e
     */
    function logoutAction(e) {
        localStorage.removeItem('user');
        login();
    }
});
