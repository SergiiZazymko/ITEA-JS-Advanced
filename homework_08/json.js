
/*
  Задание:
  Написать скрипт который:
    1. Собирает данные с формы (3 разных полей), конвертирует их в json и выводит в консоль.
  ->  2. Сделать отдельный инпут который выполняет JSON.parse(); на ту строку что вы туда ввели и выводит результат в консоль.

  Array.from(HTMLNodeColection); -> Arary

*/

window.addEventListener('DOMContentLoaded', e => {

    document.getElementById('direct').addEventListener('submit', (e) => {
        e.preventDefault();

        let form = document.getElementById('direct');
        let elements = Array.from(form.elements);

        let json = {};

            elements.forEach((element) => {
                if ('submit' !== element.type) {
                    json[element.name] = element.value;
                }
            });

            json = JSON.stringify(json);

            console.dir(json);
    });

    document.getElementById('reverse').addEventListener('submit', (e) => {
        e.preventDefault();

        let form = document.getElementById('reverse');
        let json = form.json.value;
            json = JSON.parse(json);

            console.dir(json);
    });
});
