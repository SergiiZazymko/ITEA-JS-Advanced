/*

  Задание:

    Написать при помощи async-await скрипт, который:

    Получает список компаний:  http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2
    Перебирает, выводит табличку:

    Company | Balance | Показать дату регистрации | Показать адресс |
    1. CompName 2000$ button button

    Данные о дате регистрации и адресс скрывать при выводе и показывать при клике.

*/

window.addEventListener('DOMContentLoaded', () => {
    let companies = getCompaniesList();
        companies.then(renderTable);

    /**
     *
     * @return {Promise<any>}
     */
    async function getCompaniesList() {
        let companiesList = await fetch('http://www.json-generator.com/api/json/get/ceRHciXcVu?indent=2');
            companies = await companiesList.json();

        return companies;
    }

    /**
     * `
     * @param companies
     */
    function renderTable(companies)  {
        let table = document.createElement('table');

        let header = document.createElement('tr');

            header.innerHTML = '<th>Company</th><th>Balance</th><th>Registered</th><th>Address</th>';

            table.appendChild(header);

            companies.forEach(company => {
                let tr = document.createElement('tr');

                renderSimpleTd(company.company, tr);
                renderSimpleTd(company.balance, tr);
                renderButtonTd(company.registered, tr);
                renderButtonTd(addressToString(company.address), tr);

                table.appendChild(tr);
            });

            document.body.appendChild(table);
    }

    /**
     *
     * @param data
     * @param tr
     */
    function renderSimpleTd(data, tr) {
        let td = document.createElement('td');
            td.innerHTML = data;
            tr.appendChild(td);
    }

    /**
     *
     * @param data
     * @param tr
     */
    function renderButtonTd(data, tr) {
        let td = document.createElement('td');
        let span = document.createElement('span');
            span.innerHTML = data;
            span.classList.add('hidden');
            td.appendChild(span);
            tr.appendChild(td);

        let button = document.createElement('button');
            button.innerHTML = 'Show';
            button.addEventListener('click', function(e) {
                this.classList.toggle('hidden');
                this.previousSibling.classList.toggle('hidden');
            });
            td.appendChild(button);
    }

    /**
     *
     * @param address
     * @return {string}
     */
    function addressToString(address) {
        return `${address.zip}, ${address.country}, ${address.state}, ${address.city}, ${address.street}, ${address.house}`;
    }
});
