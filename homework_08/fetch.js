/*
  Задача:

  1. При помощи fetch получить данные:
     http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2

  2. Полученый ответ преобразовать в json вызвав метод .json с объекта ответа
  3. Выбрать случайного человека и передать в следующий чейн промиса
  4. Сделать дополнительный запрос на получение списка друзей человека
     http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2
     И дальше передать обьект:
      {
        name: userName,
        ...
        friends: friendsData
      }

     Подсказка нужно вызвать дополнительный fecth из текущего чейна.
     Для того что бы передать результат выполнения доп. запроса
     в наш первый промис используйте констуркцию:

      .then(
        response1 => {
          return fetch(...)
            .then(
              response2 => {
                ...
                формируете обьект в котором будут данные человека с
                первого запроса, например его name response1.name
                и друзья которые пришли из доп. запроса
              }
            )
        }
      )

  5. Вывести результат на страничку

  + Бонус. Для конвертации обьекта response в json использовать одну
    функцию.

*/

window.addEventListener('DOMContentLoaded', () => {
   let user = 'http://www.json-generator.com/api/json/get/cgwbLkTxnS?indent=2';
   let friends = 'http://www.json-generator.com/api/json/get/bTBBXQabKG?indent=2';

    const convertToJson = result => result.json();

    const getRandomUser = usersList => usersList[Math.floor(Math.random() * usersList.length)];

    const addFriends = user => {

        const userAddFriends = friendsList => (
            {
                'name' : user.name,
                'id' : friendsList[0]._id,
                'gender' : user.gender,
                'age' : user.age,
                'friends' : friendsList[0].friends,
            }
        );

        return fetch(friends)
            .then(convertToJson)
            .then(userAddFriends);
    };

    const renderUser = user => {
        let renderedUser = `
            <h3>${user.name}</h3>
            <h4>ID: ${user.id}</h4>
            <h4>${user.gender}, ${user.age}</h4>
            Friends:
            <ul>
                ${user.friends.map(friend => `<li>${friend.name}</li>`)}
            </ul>
        `;

        let div = document.createElement('div');
            div.innerHTML = renderedUser;

            document.body.appendChild(div);
    };

   fetch(user)
       .then(convertToJson)
       .then(getRandomUser)
       .then(addFriends)
       .then(renderUser);
});
