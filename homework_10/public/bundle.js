/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./app/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./app/classes/Chicken.js":
/*!********************************!*\
  !*** ./app/classes/Chicken.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _functions_fly__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../functions/fly */ \"./app/functions/fly.js\");\n/* harmony import */ var _functions_eat__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../functions/eat */ \"./app/functions/eat.js\");\n/* harmony import */ var _functions_run__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../functions/run */ \"./app/functions/run.js\");\n\n\n\n\n/**\n *\n * @constructor\n */\nfunction Chicken() {\n    this.name = 'Chicken';\n    this.fly = _functions_fly__WEBPACK_IMPORTED_MODULE_0__[\"default\"];\n    this.eat = _functions_eat__WEBPACK_IMPORTED_MODULE_1__[\"default\"];\n    this.run = _functions_run__WEBPACK_IMPORTED_MODULE_2__[\"default\"];\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Chicken);\n\n\n//# sourceURL=webpack:///./app/classes/Chicken.js?");

/***/ }),

/***/ "./app/classes/Duck.js":
/*!*****************************!*\
  !*** ./app/classes/Duck.js ***!
  \*****************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _functions_fly__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./../functions/fly */ \"./app/functions/fly.js\");\n/* harmony import */ var _functions_eat__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./../functions/eat */ \"./app/functions/eat.js\");\n/* harmony import */ var _functions_swim__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./../functions/swim */ \"./app/functions/swim.js\");\n/* harmony import */ var _functions_sing__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./../functions/sing */ \"./app/functions/sing.js\");\n\n\n\n\n\n/**\n *\n * @constructor\n */\nfunction Duck() {\n    this.name = 'Duck';\n    this.fly = _functions_fly__WEBPACK_IMPORTED_MODULE_0__[\"default\"];\n    this.eat = _functions_eat__WEBPACK_IMPORTED_MODULE_1__[\"default\"];\n    this.swim = _functions_swim__WEBPACK_IMPORTED_MODULE_2__[\"default\"];\n    this.sing = _functions_sing__WEBPACK_IMPORTED_MODULE_3__[\"default\"];\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (Duck);\n\n\n//# sourceURL=webpack:///./app/classes/Duck.js?");

/***/ }),

/***/ "./app/classes/index.js":
/*!******************************!*\
  !*** ./app/classes/index.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _Chicken__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./Chicken */ \"./app/classes/Chicken.js\");\n/* harmony import */ var _Duck__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./Duck */ \"./app/classes/Duck.js\");\n\n\n\n/* harmony default export */ __webpack_exports__[\"default\"] = ({Chicken: _Chicken__WEBPACK_IMPORTED_MODULE_0__[\"default\"], Duck: _Duck__WEBPACK_IMPORTED_MODULE_1__[\"default\"]});\n\n//# sourceURL=webpack:///./app/classes/index.js?");

/***/ }),

/***/ "./app/functions/eat.js":
/*!******************************!*\
  !*** ./app/functions/eat.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nfunction eat() {\n    console.log(`${this.name} is eating`);\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (eat);\n\n\n//# sourceURL=webpack:///./app/functions/eat.js?");

/***/ }),

/***/ "./app/functions/fly.js":
/*!******************************!*\
  !*** ./app/functions/fly.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nfunction fly() {\n    console.log(`${this.name} is flying`);\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (fly);\n\n\n//# sourceURL=webpack:///./app/functions/fly.js?");

/***/ }),

/***/ "./app/functions/run.js":
/*!******************************!*\
  !*** ./app/functions/run.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nfunction run() {\n    console.log(`${this.name} is running`);\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (run);\n\n\n//# sourceURL=webpack:///./app/functions/run.js?");

/***/ }),

/***/ "./app/functions/sing.js":
/*!*******************************!*\
  !*** ./app/functions/sing.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nfunction sing() {\n    console.log(`${this.name} is singing`);\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (sing);\n\n\n//# sourceURL=webpack:///./app/functions/sing.js?");

/***/ }),

/***/ "./app/functions/swim.js":
/*!*******************************!*\
  !*** ./app/functions/swim.js ***!
  \*******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\nfunction eat() {\n    console.log(`${this.name} is eating`);\n}\n\n/* harmony default export */ __webpack_exports__[\"default\"] = (eat);\n\n\n//# sourceURL=webpack:///./app/functions/swim.js?");

/***/ }),

/***/ "./app/index.js":
/*!**********************!*\
  !*** ./app/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _classes___WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./classes/ */ \"./app/classes/index.js\");\n\nconsole.log(_classes___WEBPACK_IMPORTED_MODULE_0__[\"default\"]);\n\nlet {Chicken, Duck} = _classes___WEBPACK_IMPORTED_MODULE_0__[\"default\"];\n\nlet chicken = new Chicken();\n    chicken.eat();\n    chicken.fly();\n    chicken.run();\n\nlet duck = new Duck();\n    duck.eat();\n    duck.fly();\n    duck.sing();\n    duck.swim();\n\n//# sourceURL=webpack:///./app/index.js?");

/***/ })

/******/ });