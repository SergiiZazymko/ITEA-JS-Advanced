import classes from './classes/';
console.log(classes);

let {Chicken, Duck} = classes;

let chicken = new Chicken();
    chicken.eat();
    chicken.fly();
    chicken.run();

let duck = new Duck();
    duck.eat();
    duck.fly();
    duck.sing();
    duck.swim();