import fly from './../functions/fly';
import eat from './../functions/eat';
import run from './../functions/run';

/**
 *
 * @constructor
 */
function Chicken() {
    this.name = 'Chicken';
    this.fly = fly;
    this.eat = eat;
    this.run = run;
}

export default Chicken;
