import fly from './../functions/fly';
import eat from './../functions/eat';
import swim from './../functions/swim';
import sing from './../functions/sing';

/**
 *
 * @constructor
 */
function Duck() {
    this.name = 'Duck';
    this.fly = fly;
    this.eat = eat;
    this.swim = swim;
    this.sing = sing;
}

export default Duck;
