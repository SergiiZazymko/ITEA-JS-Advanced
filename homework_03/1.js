/*

  Задание написать простой слайдер.

    Есть массив с картинками из которых должен состоять наш слайдер.
    По умолчанию выводим первый элмемнт нашего слайдера в блок с id='slider'
    ( window.onload = func(){...} // window.addEventListener('load', function(){...}) )
    По клику на PrevSlide/NextSlide показываем предыдущий или следующий сладй соответствено.

    Для этого необходимо написать 4 функции которые будут:

    1. Срабатывать на событие load обьекта window и загружать наш слайд по умолчанию.
    2. RenderImage -> Очищать текущий контент блока #slider. Создавать нашу картинку и через метод аппенд чайлд вставлять её в слайдер.
    3. NextSlide -> По клику на NextSilde передавать currentPosition текущего слайда + 1 в функцию рендера
    4. PrevSlide -> Тоже самое что предыдущий вариант, но на кнопку PrevSlide и передавать currentPosition - 1
      + бонус сделать так что бы при достижении последнего и первого слада вас после кидало на первый и последний слайд соответственно.
      + бонсу анимация появления слайдов через навешивание дополнительных стилей

*/

main();

let ourSliderImages = [
    'images/cat1.jpg',
    'images/cat2.jpg',
    'images/cat3.jpg',
    'images/cat4.jpg',
    'images/cat5.jpg',
    'images/cat6.jpg',
    'images/cat7.jpg',
    'images/cat8.jpg'
];

let currentPosition = 0;

/**
 *
 * @returns void
 */
function main() {
    window.addEventListener('load', function() {
        renderImage(currentPosition);
    });

    let nextButton = document.getElementById('NextSilde');
        nextButton.addEventListener('click', nextSlide);

    let prevButton = document.getElementById('PrevSilde');
        prevButton.addEventListener('click', prevSlide);
}

/**
 *
 * @param id
 * @returns void
 */
function renderImage(id) {

    id = (-1 === id) ? ourSliderImages.length - 1 : id;
    id = (ourSliderImages.length === id) ? 0 : id;

    currentPosition = id;

    let slider = document.getElementById('slider');
        slider.innerHTML = null;

    let image = document.createElement('img');
        image.src = ourSliderImages[id];

        slider.appendChild(image);
}

/**
 *
 * @returns void
 */
function nextSlide() {
    renderImage(++currentPosition);
}

/**
 *
 * @returns void
 */
function prevSlide() {
    renderImage(--currentPosition);
}
