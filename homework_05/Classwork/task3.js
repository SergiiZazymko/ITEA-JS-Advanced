/*

    Задание 3:

    1. Создать ф-ю констурктор которая создаст новую собаку у которой есть имя и порода
    2. Обьект должен иметь пару свойств (Имя, порода)
    3. Функцию которая производит манипуляцию со свойствами (Собака бежит), (Собака есть)
    4. Функция которая перебором выводит все свойства

    // Перебор свойств и методов обьекта
    for (key in obj) {
      console.log( key, obj[key] );
      /* ... делать что-то с obj[key] ...
    // }

*/

/**
 *
 * @param name
 * @param breed
 * @constructor
 */
function Dog(name, breed) {
    this.name = name;
    this.breed = breed;
    /**
     *
     * @param status
     * @return {Dog}
     */
    this.setStatus = function(status) {
        this.status = status;
        console.log(this.name + ' is ' + this.status + 'ing');
        return this
    };
    /**
     *
     * @return {Dog}
     */
    this.showProperties = function () {
        for (let key in this) {
            console.log(key + ': ' + this[key]);
        }
        return this
    }
}

dog = new Dog('Bobik', 'taxa');
dog.setStatus('run').showProperties();
