/*

    Задание 1:

    Написать обьект Train у которого будут свойства:
    -имя,
    -скорость езды
    -количество пассажиров
    Методы:
    Ехать -> Поезд {name} везет { количество пассажиров} со скоростью {speed}
    Стоять -> Поезд {name} остановился. Скорость {speed}
    Подобрать пассажиров -> Увеличивает кол-во пассажиров на Х
*/


let Train = {
    name : 'IronMonster',
    speed : '200 km/h',
    passangersNumber : 300,
    /**
     *
     * @return {Train}
     */
    go : function() {
        console.log('Поезд ' + this.name + ' везет ' + this.passangersNumber
            + '  пассажиров со скоростью ' + this.speed);
        return this;
    },
    /**
     *
     * @return {Train}
     */
    stop: function() {
        this.speed = '0 km/h';
        console.log('Поезд ' + this.name + ' остановился, скорость ' + this.speed);
        return this;
    },
    /**
     *
     * @param number
     * @return {Train}
     */
    pickUp: function(number) {
        this.passangersNumber += number;
        console.log('Поезд ' + this.name + ' подобрал ' + number
            + ' пассажиров, теперь количество пассажиров: ' + this.passangersNumber);
        return this;
    }
};

Train.go().stop().pickUp(50);
