/*

  Задание:

    1. Написать конструктор объекта комментария который принимает 3 аргумента
    ( имя, текст сообщения, ссылка на аватаку);

    {
      name: '',
      text: '',
      avatarUrl: '...jpg'
      likes: 0
    }
      + Создать прототип, в котором будет содержаться ссылка на картинку по умлочанию
      + В прототипе должен быть метод который увеличивает счетик лайков

    var myComment1 = new Comment(...);

    2. Создать массив из 4х комментариев.
    var CommentsArray = [myComment1, myComment2...]

    3. Созадть функцию конструктор, которая принимает массив коментариев.
      И выводит каждый из них на страничку.

    <div id="CommentsFeed"></div>

*/

window.addEventListener('DOMContentLoaded', function() {

    page = new Page();
    page.run();

    /**
     *
     * @param name
     * @param text
     * @param url
     * @constructor
     */
    function Comment(name, text, url) {
        Object.setPrototypeOf(this, new AbstractComment());
        this.name = name;
        this.text = text;
        if (url) {
            this.url = url;
        }
        this.likes = 0;
    }

    /**
     *
     * @constructor
     */
    function AbstractComment() {

        this.url = 'https://hornews.com/images/news_large/c1d4b2b8ec608ea72764c5678816d5c9.jpg.pagespeed.ce.F3X__0vZO7.jpg';

        /**
         *
         * @return {AbstractComment}
         */
        this.addLike = function() {
            ++this.likes;
            return this;
        }
    }

    /**
     *
     * @param comments
     * @constructor
     */
    function CommentsFeed(comments) {
        this.comments = comments;

        /**
         *
         * @return {CommentsFeed}
         */
        this.createCommentsFees = function() {
            let commentsFeed = document.getElementById('CommentsFeed');
                commentsFeed.innerHTML = null;

            this.comments.forEach(function(comment) {

                let div = document.createElement('div');

                let img = document.createElement('img');
                    img.src = comment.url;
                    img.width = '50';
                    img.style.cssFloat = 'left';
                div.appendChild(img);

                let h3 = document.createElement('h3');
                    h3.innerHTML = comment.name;
                    h3.style.cssFloat = 'left';
                div.appendChild(h3);

                let p = document.createElement('p');
                    p.innerHTML = comment.text;
                    p.style.cssFloat = 'left';

                div.appendChild(p);

                let likes = document.createElement('p');
                    likes.innerHTML = comment.likes;
                div.appendChild(likes);

                let button = document.createElement('button');
                button.innerText = 'Like';
                    button.addEventListener('click', function(e) {
                        comment.addLike();
                        let p = button.previousElementSibling;
                            p.innerHTML = comment.likes;
                    });

                div.appendChild(button);

                commentsFeed.appendChild(div);

            });

            return this;
        }
    }

    /**
     *
     * @constructor
     */
    function Page() {
        this.comments = [];
        this.form = document.getElementById('comment_form');

        /**
         *
         * @param e
         * @return {Page}
         */
        this.addComment = function(e) {
            e.preventDefault();

            let comment = new Comment(e.target.name.value, e.target.text.value, e.target.url.value);

            this.comments.push(comment);

            let commentsFeed = new CommentsFeed(this.comments);
                commentsFeed.createCommentsFees();

            return this;
        };

        this.addComment = this.addComment.bind(this);

        /**
         *
         * @return {Page}
         */
        this.run = function() {
            this.form.addEventListener('submit', this.addComment);

            return this;
        };
    }
});
